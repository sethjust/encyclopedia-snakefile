msconvert = "docker run -it --rm -e WINEDEBUG=-all -v .:/data chambm/pwiz-skyline-i-agree-to-the-vendor-licenses wine msconvert"

#encyclopedia = "java -jar encyclopedia-0.9.5-executable.jar"
encyclopedia = "~/src/encyclopedia-native/target/encyclopedia-native"

library = "20200315_QEHFX_lkp_silacdia_heavy_dia_006.mzML.elib"
fasta = "uniprot-proteome_UP000005640+reviewed_yes.fasta"

rule all:
  input: "20191104_QEHFX_lkp_pSILAC-DIA_A_1-0ug_032.mzML.elib"

rule convert:
  input: "{sample}.raw"
  output: "protected({sample}.mzML)"
  shell: "{msconvert} --mzML --mz64 --inten32 --filter 'peakPicking vendor msLevel=1-' --filter 'demultiplex optimization=overlap_only' -o /data/ --outfile {output:q} /data/{input:q}"

#TODO: rule for creating .dia

rule search:
  input: "{sample}.mzML"
  output: "{sample}.mzML.elib"
  threads: workflow.cores
  shell: "{encyclopedia} -numberOfThreadsUsed {threads} -l {library:q} -f {fasta:q} -i {input:q} -frag YONLY"
