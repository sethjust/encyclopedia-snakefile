# encyclopedia-snakefile

This is a proof-of-concept example of using Snakemake to execute `msconvert`
and EncyclopeDIA, presented as an example of how these tools can be used in
concert.

You may reference this code as an example but are not permitted to copy it
verbatim for any reason. This code will very likely not work for you as written
and the authors offer no guarantee that it will work at all.
