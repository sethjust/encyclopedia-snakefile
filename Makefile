.PHONY: all dry-run clean

all:
	snakemake --cores

dry-run:
	snakemake -npr

clean:
	snakemake --cores --dry-run --delete-all-output --verbose
